/**
 * FileName:	NumArray.java
 *
 * 作者:			元兒～
 * 
 * 更新資訊: 
 * v0.0 2012.12.6 完成主要功能
 * - 完成建構子, reset, getNumber, getNumber_selected, getSelectedTotal, selectNumber, setNumber_selected
 * v0.1 2012.12.8
 * - 修改建構子成為public
 * - 新增getTotal
 * v0.2 2012.12.14
 * - 修改程式內部的userNum都從1開始計算，非從0！！
 * v0.3 2012.12.14
 * - 建表: userNum_toIndex: 以數字內容來取得索引值
 * - 新增以數字內容控制selectNumber(int), setNumber_selected(int, boolean)
 * v0.4 2012.12.21
 * - 將所有方法都加上以數字搜尋功能
 * v0.5 2012.12.24
 * - 加入使用者參數費為範圍判斷（防呆）
 * v0.6 2012.12.24
 * - 移除無參數建構子
 * v0.7 2012.12.24
 * - 修正getNumber_selected(int), selectNumber(int), setNumber_selected(int, boolean)無法輸入最大值本身數字
 * v0.8 2012.12.28
 * - 將reset()內容轉移到resetData()
 * v0.9 2013.1.1
 * - 將選取功能移開成獨立的SelectNumAttay
 * 
 * Description: 
 * 	這個物件是代表這個使用者持有的數字盤
 *
 */
package game.bingo;
import game.rand.num.NumRand;

public class NumArray 
{
	private int userNumRow_total,userNumCol_total;	//抽取範圍
	private int userNum_total;	//抽取範圍
	private int[][] userNum; //userNum[x][y]=使用者表上的數字
	private int[] userNum_toIndex;	//建表: userNum_toIndex[數字-1]=x*getTotal(COL)+y（所以值）
	private NumRand rand; //使用取亂數用的
	
	//常數指定
	public final static int ROW = 1; 
	public final static int COL = 2; 
	public final static int TOTAL = 3; 
	
	public NumArray(int input_userNumRow_total,int input_userNumCol_total)
	{
		//把參數代入的寫進變數裡
		this.userNumRow_total = input_userNumRow_total;
		this.userNumCol_total = input_userNumCol_total;
		this.userNum_total = this.userNumRow_total*this.userNumCol_total;
		
		//建立使用者表上的數字陣列空間
		userNum = new int[userNumRow_total][];
		for(int i=0; i<userNumRow_total; i++)
			userNum[i] = new int[userNumCol_total];
		userNum_toIndex = new int[userNum_total];
		
		//建立取亂數用的物件
		rand = new NumRand(userNum_total);
		
		resetData();	//初始化變數內容
		
	}
	
	//內部重設
	private void resetData(){
		rand.shuffle();	//抽籤物件重設
		
		//分配抽到的數字到userNum
		for(int i=0; i<userNumRow_total; i++){
			for(int j=0; j<userNumCol_total; j++){
				userNum[i][j] = rand.getNumber(userNum_total,true)+1;	//取得亂數得到的到userNum
				userNum_toIndex[ userNum[i][j]-1 ] = i*getTotal(COL)+j;	//寫進索引到userNum_toIndex
			}
		}
	}
	//重設（通常用於重新開始遊戲時）
	public void reset(){
		resetData();
	}
	
	//重新整理
	public void refresh(){
		//重新將行列total計算到userNum_total
		this.userNum_total = this.userNumRow_total*this.userNumCol_total;
	}
	
	//取得這個數字盤有多大
	public int getTotal(int choose){
		switch(choose){
		case 1:
			return userNumRow_total;
		case 2:
			return userNumCol_total;
		case 3:
			return userNum_total;
		}
		return 0;
	}
	
	//取得這個數字
	public int getNumber(int inputRow, int inputCol){
		//如果代入的參數座標是在範圍內
		if( (inputRow>=0 && inputRow<userNumRow_total) && (inputCol>=0 && inputCol<userNumCol_total) ){
			return userNum[inputRow][inputCol];
		}
		else return -1;
	}
	public int getNumber_toIndex(int num, int choose){
		switch(choose){
		case 1:
			return userNum_toIndex[num-1]/getTotal(COL);
		case 2:
			return userNum_toIndex[num-1]%getTotal(COL);
		}
		return -1;
	}
}
