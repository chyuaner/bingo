/**
 * FileName:	Bingo.java
 * 
 * 更新資訊: 
 * v0.0 2012.12.14
 * v1.0 2012.12.21
 *   - 新增連線對照表與無參數建構子   
 * v1.1 2012.12.24
 *   - 修改Bingo建構子（因為賓果遊戲一定是正方格子）
 * v1.2 2012.12.24
 *   - 移除無參數建構子
 *   - 修改喊號優先順序表宣告
 * v1.3 2012.12.24
 *   - 建立機率表
 * v1.4 2012.12.27
 * - 設定機率表初始值: 中間為2（含偶數格子）、四個角為1
 * v1.4-pre-alpha
 * - 修改架構（新增重設機率資料方法）（有Bug）
 * - 新增取得建議數字maxChanceBoard
 * v1.5
 * - 解決因父類別呼叫reset而出現未分配chanceBoard空間的問題
 * v1.6 2013.1.7
 * - 新增處理喊號的功能實作，以maxChanceBoard為參考
 * v1.7 2013.1.7
 * - 實作計算有幾條連成線
 * v1.8 2013.1.7
 * - 加入如果"已全部都選取"的判斷來回傳機率值
 * 
 * Description: 
 * 	這個類別是被Bingo繼承，判斷是否連成線
 *
 */

package game.bingo;
public class Bingo extends SelectNumArray
{
	private int[][] chanceBoard;	//喊號優先順序表
	
	private int maxChanceBoard_num;	//最高的機率值
	private int maxChanceBoard_row, maxChanceBoard_col;
	
	private int isLineCount;	//有幾條連成線

	private void resetData(){
		//指定機率初始值（中間為2，角落為1）
		for(int i=0; i<getTotal(NumArray.ROW); i++){
			//將初始值設定為0
			for(int j=0; j<getTotal(NumArray.COL); j++) chanceBoard[i][j] = 0;
			
			//若是第1行or最後1行
			if(i==0 || i==getTotal(NumArray.ROW)-1){
				//將最左邊和最右邊標示為1（標示角落為1）
				chanceBoard[i][0] = 1;
				chanceBoard[i][getTotal(NumArray.COL)-1] = 1;
			}
			//將中間標示為2
			else if(i==getTotal(NumArray.ROW)/2){
				chanceBoard[i][getTotal(NumArray.COL)/2] = 2;
				if(getTotal(NumArray.ROW)%2 == 0) chanceBoard[i][getTotal(NumArray.COL)/2-1] = 2;
			}
			//若方格數是偶數
			else if(i==getTotal(NumArray.ROW)/2-1 && getTotal(NumArray.ROW)%2 == 0){
				chanceBoard[i][getTotal(NumArray.COL)/2-1] = 2;
				chanceBoard[i][getTotal(NumArray.COL)/2] = 2;
			}
		}
		
		//重設最高的機率值
		maxChanceBoard_num = 2;
		//若方格數是偶數，標示中間座標
		if(getTotal(NumArray.ROW)%2 == 1){
			maxChanceBoard_row = getTotal(NumArray.ROW)/2;
			maxChanceBoard_col = getTotal(NumArray.COL)/2;
		}
		//若方格數是偶數
		else if(getTotal(NumArray.ROW)%2 == 0){
			maxChanceBoard_row = getTotal(NumArray.ROW)/2-1;
			maxChanceBoard_col = getTotal(NumArray.COL)/2-1;
		}
		
		//重設已連成幾條線
		isLineCount = 0;
	}
	
	//重新掃一遍哪格是機率值最高的
	private void scan_maxChanceBoard(){
		boolean isUpdateed = false;	//有修改到機率值嗎？
		maxChanceBoard_num = 0;	//歸零
		for(int i=0; i<getTotal(NumArray.ROW); i++){
			for(int j=0; j<getTotal(NumArray.COL); j++){
				if(updateAdd_maxChanceBoard(i,j))	//累加機率值
					isUpdateed = true;	//如果有修改到的話則true
			}
		}
		//如果已經全部都被選過的話
		if(!isUpdateed){
			maxChanceBoard_num = -1;
			maxChanceBoard_row = -1;
			maxChanceBoard_col = -1;
		}
	}
	
	//若找到機率值更高，則累加
	private boolean updateAdd_maxChanceBoard(int inputRow, int inputCol){
		//迴避被選取的位置
		if(!getNumber_selected(inputRow, inputCol)){
			//如果當前機率值比當前最大機率值還大的話
			if(chanceBoard[inputRow][inputCol] > maxChanceBoard_num){
				maxChanceBoard_num = chanceBoard[inputRow][inputCol];
				maxChanceBoard_row = inputRow;
				maxChanceBoard_col = inputCol;
				return true;	//有更動，回傳true
			}
			else return false;	//無更動，回傳false
		}
		else return false;	//無更動，回傳false
	}
	
	public Bingo(int input_userNum_total) 
	{
		super(input_userNum_total, input_userNum_total);
		
		//建立喊號優先順序表
		chanceBoard = new int[getTotal(NumArray.ROW)][];
		for(int i=0; i<getTotal(NumArray.ROW); i++) chanceBoard[i] = new int[getTotal(NumArray.COL)];
		resetData();
	}

	//回傳要喊的下一個號碼
	public int getSuggestNumber() 
	{
		if(maxChanceBoard_num != -1)
			return getNumber(maxChanceBoard_row, maxChanceBoard_col);
		else return -1;
	}
	public int getSuggestNumber_toIndex(int choose){
		switch(choose){
		case 1:
			return maxChanceBoard_row;
		case 2:
			return maxChanceBoard_col;
		}
		return 0;
	}
	//DEBUG-回傳機率值
	public int getMaxChanceBoard(){
		return maxChanceBoard_num;
	}
	//回傳已成功連成幾條線
	public int getIsLine()
	{
		return isLineCount;
	}
	
	
	//覆寫reset方法: 重設（通常用於重新開始遊戲時）
	public void reset(){
		super.reset();
		resetData();
	}
	//覆寫selectNumber方法: 在呼叫此方法時，一併計算機機率
	public boolean selectNumber(int inputNum){
		//如果代入的數字是在範圍內
		if(inputNum <= getTotal(NumArray.TOTAL)){
			selectNumber(getNumber_toIndex(inputNum,NumArray.ROW),getNumber_toIndex(inputNum,NumArray.COL));
			return true;
		}
		else return false;
	}
	public boolean selectNumber(int inputRow, int inputCol){
		//若尚未被選取的話
		if(!getNumber_selected(inputRow, inputCol)){
			//選取這個數
			if(super.setNumber_selected(inputRow, inputCol, true)){
				boolean isAddChanceBoardCount = false;	//有累加到機率值嗎？
				int selectNumStraight = 0;	//檢查是否已經連成線的計數用
				
				//橫向計算
				for(int i=0; i<getTotal(NumArray.COL); i++){
					chanceBoard[inputRow][i]++;
					if(updateAdd_maxChanceBoard(inputRow, i))	//累加更高機率值
						isAddChanceBoardCount = true;	//有累加到機率值，則指定成已加過
					if(getNumber_selected(inputRow, i)) selectNumStraight++;	//如果有選取的話則計數器+1
				}
				if(selectNumStraight == getTotal(COL)) isLineCount++;	//已經連成一條線，則已連成線數+1
				//直向計算
				selectNumStraight = 0;
				for(int i=0; i<getTotal(NumArray.ROW); i++){
					chanceBoard[i][inputCol]++;
					if(updateAdd_maxChanceBoard(i, inputCol))	//累加更高機率值
						isAddChanceBoardCount = true;	//有累加到機率值，則指定成已加過
					if(getNumber_selected(i, inputCol)) selectNumStraight++;	//如果有選取的話則計數器+1
				}
				if(selectNumStraight == getTotal(ROW)) isLineCount++;	//已經連成一條線，則已連成線數+1
				//左上右下-斜線處理
				selectNumStraight = 0;
				if(inputRow == inputCol){	//是否在斜線上
					for(int i=0; i<getTotal(NumArray.ROW); i++){
						chanceBoard[i][i]++;
						if(updateAdd_maxChanceBoard(i, i))	//累加更高機率值
							isAddChanceBoardCount = true;	//有累加到機率值，則指定成已加過
						if(getNumber_selected(i, i)) selectNumStraight++;	//如果有選取的話則計數器+1
					}
					if(selectNumStraight == getTotal(ROW)) isLineCount++;	//已經連成一條線，則已連成線數+1
				}
				//右上左下-斜線處理
				selectNumStraight = 0;
				if(inputRow == getTotal(NumArray.ROW)-inputCol-1){	//是否在斜線上
					for(int i=0; i<getTotal(NumArray.ROW); i++){
						chanceBoard[i][getTotal(NumArray.ROW)-i-1]++;
						if(updateAdd_maxChanceBoard(i, getTotal(NumArray.ROW)-i-1))	//累加更高機率值
							isAddChanceBoardCount = true;	//有累加到機率值，則指定成已加過
						if(getNumber_selected(i, getTotal(NumArray.ROW)-i-1)) selectNumStraight++;	//如果有選取的話則計數器+1
					}
					if(selectNumStraight == getTotal(ROW)) isLineCount++;	//已經連成一條線，則已連成線數+1
				}
				
				//如果都沒加到任何機率值的話
				if(!isAddChanceBoardCount) scan_maxChanceBoard();
				
				return true;
			}
			else return false;	//選取失敗回傳
		}
		else return false;	//已被選過，回傳失敗
	}
	
	//取得機率數字
	public int getChanceNumber(int inputRow, int inputCol){
		if( (inputRow>=0 && inputRow<getTotal(NumArray.ROW)) && (inputCol>=0 && inputCol<getTotal(NumArray.COL)) ){
			return chanceBoard[inputRow][inputCol];
		}
		else return -1;
	}
}
