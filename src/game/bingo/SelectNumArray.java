/**
 * FileName:	SelectNumArray.java
 *
 * 作者:			元兒～
 * 
 * 更新資訊: 
 * v0.0 2013.1.1
 * - 將選取功能移開成獨立的SelectNumAttay
 *
 */
package game.bingo;


public class SelectNumArray extends NumArray {
	
	private boolean[][] userNum_selected; //userNum_selected[第?個按鈕]=是否已被選取
	private int userNum_selected_total = 0; //總共有?個數字被選取
	
	
	public SelectNumArray(int input_userNumRow_total, int input_userNumCol_total) {
		super(input_userNumRow_total, input_userNumCol_total);

		//建立使用者表上的數字有?個數字被翻選取陣列空間
		userNum_selected = new boolean[getTotal(NumArray.ROW)][];
		for(int i=0; i<getTotal(NumArray.ROW); i++)
			userNum_selected[i] = new boolean[getTotal(NumArray.COL)];
		
		resetData();
	}
	
	//內部重設
	private void resetData(){
		//將選取的總計重設成0
		userNum_selected_total = 0;
		
		//把全部數字設定成非選取狀態
		for(int i=0; i<getTotal(NumArray.ROW); i++)
			for(int j=0; j<getTotal(NumArray.COL); j++) userNum_selected[i][j] = false;
	}
	//重設
	public void reset(){
		super.reset();
		resetData();
	}
	
	//重新整理
	public void refresh(){
		super.refresh();
		
		//重新從userNum前到後整理對應資料
		userNum_selected_total = 0; //將選取的總計重設成0
		
		for(int i=0; i<getTotal(NumArray.ROW); i++){
			for(int j=0; j<getTotal(NumArray.COL); j++){
				if(userNum_selected[i][j]) userNum_selected_total++;	//重新計算正確的已選取數
			}
		}
	}
	
	//取得這個數字有無被選取
	public boolean getNumber_selected(int inputNum){
		//如果代入的數字是在範圍內
		if(inputNum <= getTotal(NumArray.TOTAL)){
			return userNum_selected[getNumber_toIndex(inputNum, ROW)][getNumber_toIndex(inputNum, COL)];
		}
		else return false;
	}
	public boolean getNumber_selected(int inputRow, int inputCol){
		//如果代入的參數座標是在範圍內
		if( (inputRow>=0 && inputRow<getTotal(NumArray.ROW)) && (inputCol>=0 && inputCol<getTotal(NumArray.COL)) ){
			return userNum_selected[inputRow][inputCol];
		}
		else return false;
	}
	//取得總共有被選取的量
	public int getSelectedTotal(){return userNum_selected_total;}
	
	//選取or取消選取這個數字
	public boolean selectNumber(int inputNum){
		//如果代入的數字是在範圍內
		if(inputNum <= getTotal(NumArray.TOTAL)){
			return selectNumber(getNumber_toIndex(inputNum,ROW),getNumber_toIndex(inputNum,COL));
		}
		else return false;
	}
	public boolean selectNumber(int inputRow, int inputCol){
		//如果代入的參數座標是在範圍內
		if( (inputRow>=0 && inputRow<getTotal(NumArray.ROW)) && (inputCol>=0 && inputCol<getTotal(NumArray.COL)) ){
			
			//若這個數尚未被選取
			if(userNum_selected[inputRow][inputCol] == false){
				userNum_selected[inputRow][inputCol] = true;	//選取這個數
				userNum_selected_total++;	//設定總計選取數字的量+1
			}
			//若這個數已被選取
			else if(userNum_selected[inputRow][inputCol] == true){
				userNum_selected[inputRow][inputCol] = false;	//取消選取這個數
				userNum_selected_total--;	//設定總計選取數字的量-1
			}
			
			return true;
		}
		//如果代入的參數座標非在範圍內
		else return false;
	}
	
	//直接指定這個數字有無被選取
	public boolean setNumber_selected(int inputNum, boolean inputTF){
		//如果代入的數字是在範圍內
		if(inputNum <= getTotal(NumArray.TOTAL)){
			return setNumber_selected(getNumber_toIndex(inputNum,ROW), getNumber_toIndex(inputNum,COL), inputTF);
		}
		else return false;
	}
	public boolean setNumber_selected(int inputRow, int inputCol, boolean inputTF){
		//如果代入的參數座標是在範圍內
		if( (inputRow>=0 && inputRow<getTotal(NumArray.ROW)) && (inputCol>=0 && inputCol<getTotal(NumArray.COL)) ){
			
			//若使用者指定為true，且這個數尚未被選取
			if(inputTF == true && userNum_selected[inputRow][inputCol] == false){
				userNum_selected[inputRow][inputCol] = true;	//選取這個數
				userNum_selected_total++;	//設定總計選取數字的量+1
			}
			//若使用者指定為false，且這個數已被選取
			else if(inputTF == false && userNum_selected[inputRow][inputCol] == true){
				userNum_selected[inputRow][inputCol] = false;	//取消選取這個數
				userNum_selected_total--;	//設定總計選取數字的量-1
			}
			
			return true;
		}
		//如果代入的參數座標非在範圍內
		else return false;
	}
}