/**
* 類別名稱：BingoLayout
* 
* 更新資訊: 
* v0.1
* - 修改架構，調整建立介面的method
* - 實作數字盤顯示功能
* 
* v0.2 2012.12.14
* - 將button1改名→number_button
* - 將number_button改成二維陣列
* - 新增ActionListener事件: 按下數字盤上的數字按鈕
* - 將Bingo改名→bingoNum
* - 新增一個TextField、一個按鈕和一個Label到視窗上
* - 刪除 path 這個變數和ImegeIcon這個物件
* - 將newGame()和updateUI()的存取模式改為私有存取(private)
* 
* v0.3 2012.12.21
* - 設定inputNumber成只能輸入數字
* - 改成按下棋盤按鈕後，對應到物件的選取
* 
* v0.4 2012.12.21
* - 修改建立視窗元件架構以及更改物件名稱
* - 指定inputButton動作
* v0.5 2012.12.24
* - 加入inputNumber防呆
* - 加入結束功能
* v0.4.1 2012.12.24
* - 修改Bingo建構子對應（因為賓果遊戲一定是正方格子）
* v0.6 2012.12.24
*  -修改updateUI()裡的東西(在第231行)
* v0.7 2012.12.24
* - 增加: 在使用者輸入選取數字時，按下Enter鍵亦可執行
* v0.8 2012.12.28
* - 更改輸入數字、喊號的變數名稱及對應
* v0.9 2013 01.02
* - 新增RadioButton和ButtonGroup，用來轉換狀態(對方先還是我們先)
* v1.0 2013.1.7
* - 修改介面排版以及功能對應
* v1.1 2013.1.8
* - 誰先誰後最佳化（鎖定操作）
* v1.2 2013.1.8
* - 自動選號
* 
* 說明:
* 此類別是建立視窗應用程式的類別。
*/

import game.bingo.*;
import java.awt.*;

import javax.swing.*;
import javax.swing.text.*;

import java.awt.event.*;

@SuppressWarnings("serial")
class NumberDocument extends PlainDocument
{  
	/**
	 * [JAVA]控制JTextField得輸入為數字
	 *  http://ned0666.pixnet.net/blog/post/50127215-%5Bjava%5D%E6%8E%A7%E5%88%B6jtextfield%E5%BE%97%E8%BC%B8%E5%85%A5%E7%82%BA%E6%95%B8%E5%AD%97
	 *  描述：使用格式JTextField.setDocument(new   NumberDocument())即可  
	*/
    public void insertString(int offs,String str,AttributeSet a)throws BadLocationException   
    {  
        char[] source = str.toCharArray();  
        char[] result = new char[source.length];  
        int j = 0;  
        for(int i = 0;i < result.length;i++)
        {
            if(Character.isDigit(source[i]))//是數字  
                result[j++] = source[i];  
            else//不是數字  
                Toolkit.getDefaultToolkit().beep();  //發出警示音效
        }
        super.insertString(offs , new String(result,0,j) , a);  
    }  
} 

@SuppressWarnings({ "unused", "serial" })
public class BingoLayout extends JFrame implements ActionListener
{
	//資料物件宣告
	private Bingo bingoNum;
	
	//介面物件宣告
	private Container c = getContentPane();
	private JPanel bingoNumber_panel = new JPanel();
	private JPanel control_panel = new JPanel();
	private JPanel input_panel = new JPanel();
	private JPanel status_panel = new JPanel();
	private JPanel firstTurn_panel = new JPanel();
	
	private JMenuBar menubar = new JMenuBar();
	private JMenu game_menu,help_menu;
	private JMenuItem startGame_menuItem,exitGame_menuItem,item2;
	
	private JRadioButton we_first,Other_side;
	private ButtonGroup firstTurn_buttonGroup = new ButtonGroup();
	
	private JLabel inputNumber_label = new JLabel("輸入數字：");
	private JTextField inputNumber_textField = new JTextField(12);
	private JButton inputNumber_button = new JButton("確定");
	
	private JLabel hint_seeLabel = new JLabel("我方喊號: ");
	private JLabel hint_label = new JLabel();
	private JLabel isLine_seeLabel = new JLabel("連線數: ");
	private JLabel isLine_label = new JLabel();
	
	private JButton[][] number_button;
	
	public BingoLayout()
	{
		bingoNum = new Bingo(5);	//指定要?*?的方格
		
		//super("Bingo 遊戲");
		this.setTitle("Bingo 遊戲"); //設定視窗標題
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);	//設定按下視窗關閉紐就釋放這個物件
		this.setSize(400,400);	//status指定視窗大小
		this.setVisible(true);	//將視窗顯示出來
		
		defineUiObject();
		addUiObject();
		newGame();
		updateUI();
	}
	
	//定義介面上的物件
	private void defineUiObject()
	{
		//功能表
		setJMenuBar(menubar);
		game_menu = new JMenu("遊戲(G)");
		game_menu.setMnemonic(KeyEvent.VK_G);
		startGame_menuItem = new JMenuItem("新遊戲(S)",KeyEvent.VK_S);
		startGame_menuItem.addActionListener(this);
		exitGame_menuItem = new JMenuItem("結束(Q)",KeyEvent.VK_Q);
		exitGame_menuItem.addActionListener(this);

		help_menu = new JMenu("關於");
		help_menu.addActionListener(this);
		
		//控制介面區
		control_panel.setLayout( new GridLayout(3, 1) );
		
		//誰先誰後轉換區
		we_first = new JRadioButton("我們先(W)");
		we_first.setMnemonic(KeyEvent.VK_W);
		we_first.addActionListener(this);
		Other_side = new JRadioButton("對方先(O)");
		Other_side.setMnemonic(KeyEvent.VK_O);
		Other_side.addActionListener(this);
		
		//狀態顯示區
		status_panel.setLayout( new FlowLayout() );
		
		//輸入數字區
		input_panel.setLayout( new FlowLayout() );
		inputNumber_textField.setDocument( new NumberDocument() );	//設定成只能輸入數字（有用到自建類別NumberDocument）
		//inputNumber.setColumns(2);
		inputNumber_textField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				//在使用者輸入選取數字時，按下Enter鍵亦可執行
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					inputSelectNumber();
				}
			}
		});
		inputNumber_button.addActionListener(this);
		
		//棋盤區
		number_button = new JButton[bingoNum.getTotal(Bingo.ROW)][];
		for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
		{
			number_button[i] = new JButton[bingoNum.getTotal(Bingo.COL)];
			for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++)
			{
				number_button[i][j] = new JButton();
				number_button[i][j].addActionListener(this);
			}
		}
			
		bingoNumber_panel.setLayout(new GridLayout(bingoNum.getTotal(Bingo.ROW),bingoNum.getTotal(Bingo.COL),0,0));
	}

	//實際在視窗上新增元件
	private void addUiObject()
	{
		//功能表
		menubar.add(game_menu);
		game_menu.add(startGame_menuItem);
		game_menu.add(exitGame_menuItem);
		menubar.add(help_menu);
		
		//控制介面區
		c.add(control_panel, BorderLayout.NORTH);
		
		//誰先誰後轉換區
		control_panel.add(firstTurn_panel);
		firstTurn_panel.add(we_first);
		firstTurn_panel.add(Other_side);
		firstTurn_buttonGroup.add(we_first);
		firstTurn_buttonGroup.add(Other_side);
		
		//狀態顯示區
		control_panel.add(status_panel);
		status_panel.add(hint_seeLabel);
		status_panel.add(hint_label);
		status_panel.add(isLine_seeLabel);
		status_panel.add(isLine_label);
		
		//輸入數字區
		control_panel.add(input_panel);
		input_panel.add(inputNumber_label);
		input_panel.add(inputNumber_textField);
		input_panel.add(inputNumber_button);
		
		//棋盤按鈕按鈕
		c.add(bingoNumber_panel,BorderLayout.CENTER);
		for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
		   for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++) bingoNumber_panel.add(number_button[i][j]);
	}
	
	//視窗按鈕動作
	public void actionPerformed(ActionEvent e)
	{
		//功能表
		if(e.getSource() == startGame_menuItem) newGame();
		else if(e.getSource() == exitGame_menuItem) System.exit(0);
		//輸入數字區
		else if(e.getSource() == inputNumber_button) inputSelectNumber();
		//誰先誰後選擇區
		else if(e.getSource() == we_first) 
		{

			//將誰先誰後的選項鎖定
			we_first.setEnabled(false);
			Other_side.setEnabled(false);
			
			//數字輸入框
			inputNumber_textField.setText("");
			inputNumber_textField.setEnabled(true);
			inputNumber_button.setEnabled(true);
			
			//將棋盤按鈕設定成可按下的
			for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
				for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++)
					number_button[i][j].setEnabled(true);
			hint_label.setText(String.valueOf( bingoNum.getSuggestNumber() ));
			
			//JOptionPane.showMessageDialog(we_first,"we First!!"); //測試用
			if(!bingoNum.getNumber_selected(bingoNum.getSuggestNumber_toIndex(Bingo.ROW), bingoNum.getSuggestNumber_toIndex(Bingo.COL)))
			{
				number_button[bingoNum.getSuggestNumber_toIndex(Bingo.ROW)][bingoNum.getSuggestNumber_toIndex(Bingo.COL)].setBackground(Color.LIGHT_GRAY);
				bingoNum.selectNumber(bingoNum.getSuggestNumber_toIndex(Bingo.ROW), bingoNum.getSuggestNumber_toIndex(Bingo.COL));
			}
		}

		else if(e.getSource() == Other_side){
			//將誰先誰後的選項鎖定
			we_first.setEnabled(false);
			Other_side.setEnabled(false);
			
			//數字輸入框
			inputNumber_textField.setText("");
			inputNumber_textField.setEnabled(true);
			inputNumber_button.setEnabled(true);
			
			//將棋盤按鈕設定成可按下的
			for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
				for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++)
					number_button[i][j].setEnabled(true);
			
			//JOptionPane.showMessageDialog(Other_side, "Other side!!"); //測試用
		}
		//棋盤按鈕區
		else
		{
			//按下數字盤上的數字按鈕
			for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
			{
				for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++)
				{
					if(e.getSource() == number_button[i][j])
					{
						selectNumberButton(i,j);
					}
				}
			}
		}
	}
	
	//選取數字按鈕
	private boolean selectNumberButton(int inputRow, int inputCol){
		boolean selected = false;
		if( bingoNum.selectNumber(inputRow, inputCol) ){	//設定選取
			int autoSelectNumber = bingoNum.getSuggestNumber();
			hint_label.setText(String.valueOf(autoSelectNumber));
			bingoNum.selectNumber(autoSelectNumber);
			
			selected = true;
			updateUI();	//更新介面
		}
		return selected;
	}
	
	
	//重新開始遊戲
	private void newGame()
	{
		firstTurn_buttonGroup.clearSelection();	//取消誰先誰後的選取
		//將誰先誰後的選項解除鎖定
		we_first.setEnabled(true);
		Other_side.setEnabled(true);
		
		//將棋盤按鈕設定成鎖定的
		for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
			for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++)
				number_button[i][j].setEnabled(false);
		
		//數字輸入框
		inputNumber_textField.setText("");
		inputNumber_textField.setEnabled(false);
		inputNumber_button.setEnabled(false);
		
		//遊戲重新開始
		bingoNum.reset();
		updateUI();
	}
	//更新介面所有內容
	private void updateUI()
	{
		//將所有數字盤輸出到按鈕上
		for(int i=0; i<bingoNum.getTotal(Bingo.ROW); i++)
		{
			for(int j=0; j<bingoNum.getTotal(Bingo.COL); j++)
			{
				number_button[i][j].setText( String.valueOf( bingoNum.getNumber(i, j) ) );
				if(bingoNum.getNumber_selected(i, j)) number_button[i][j].setBackground(Color.LIGHT_GRAY);
				else number_button[i][j].setBackground(null);
			}
		}
		//hint_label.setText(String.valueOf( bingoNum.getSuggestNumber() ));
		isLine_label.setText(String.valueOf( bingoNum.getIsLine() ));
		firstTurn_buttonGroup.clearSelection();
	}
	//從使用者輸入數字來選取
	private void inputSelectNumber(){
		try
		{
			//若使用者輸入的數在範圍內
			if(Integer.parseInt(inputNumber_textField.getText()) > 0 && Integer.parseInt(inputNumber_textField.getText()) <= bingoNum.getTotal(Bingo.TOTAL))
			{
				selectNumberButton(
						bingoNum.getNumber_toIndex(Integer.parseInt( inputNumber_textField.getText() ), Bingo.ROW),
						bingoNum.getNumber_toIndex(Integer.parseInt( inputNumber_textField.getText() ), Bingo.COL)
						);	//設定按鈕選取
				inputNumber_textField.setText("");
			}
			else
				JOptionPane.showMessageDialog(inputNumber_button, "你輸入的數字不在範圍內喔");	//跳出錯誤訊息提示視窗
		}
		catch (IllegalArgumentException ex) 
		{
			JOptionPane.showMessageDialog(inputNumber_button, "你輸入的不是數字喔");	//跳出錯誤訊息提示視窗
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(inputNumber_button, "其他錯誤");	//跳出錯誤訊息提示視窗
		}
	}
}
