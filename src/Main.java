/*
* 類別名稱：Main
* 說明：
* 此類別為JAVA視窗應用程式的進入點，不需做修改
*/

@SuppressWarnings("unused")
public class Main
{	
	public static void main(String[] args)
	{
		BingoLayout bingoLayout = new BingoLayout();
	}
}